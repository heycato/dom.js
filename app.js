var dom = require('./dom');
(function() {
	var renderLoop = () => {
		requestAnimationFrame(renderLoop);
		dom.renderUpdates();
	};
	renderLoop();
}());

var body = dom.createElement(document.body);
var roundRobin = arr => {
	var len = arr.length;
	var curI = -1;
	return () => {
		if(curI < len - 1) {
			curI++;
		} else {
			curI = 0;
		}
		return arr[curI];
	};
};
var styles = [
	{
		backgroundColor:'blue'
	},
	{
		backgroundColor:'red'
	},
	{
		backgroundColor:'green'
	},
	{
		backgroundColor:'yellow'
	},
	{
		backgroundColor:'purple'
	},
];

var sizes = [
	[50,50],
	[100,100],
	[200,200],
	[400,400],
	[800,800],
];

var positions = sizes.reverse();

var getNextStyle = roundRobin(styles);
var getNextSize = roundRobin(sizes);
var getNextPosition = roundRobin(positions);


var getElement = () => {
		var el = dom.createElement('div');
		var pos = getNextPosition();
		var sz = getNextSize();
		function removeElement() {
			body.removeChild(el);
			el.recycle();
		}
		el.setStyle({
			transition:'all 1s',
			position:'absolute',
			display:'block'
		});
		el.setStyle(getNextStyle());
		el.setX(pos[0]);
		el.setY(pos[1]);
		el.setWidth(sz[0]);
		el.setHeight(sz[1]);
		el.addEventListener('click', removeElement);
		return el;
	};
var div;

window.addEventListener('click', e => {
	console.log('INACTIVE_ELEMENT_REPOSITORY');
	console.group();
	console.log(dom.getInactiveRepo());
	console.groupEnd();
	console.log('ACTIVE_ELEMENT_REPOSITORY');
	console.group();
	console.log(dom.getActiveRepo());
	console.groupEnd();
	console.log('PENDING_UPDATES_QUEUE');
	console.group();
	console.log(dom.getActiveRepo());
	console.groupEnd();
	div = getElement();	
	body.appendChild(div);
	console.log('div =', div);
});

