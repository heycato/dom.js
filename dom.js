/*
 * @name INACTIVE_ELEMENT_REPOSITORY
 * @description Memory pool object for inactive dom element definitions.
 * @example
 *
 * 	an inactive div element:
 *
 * 	{
 * 		div:[[<key>, <definition>]]
 * 	}
 */
var INACTIVE_ELEMENT_REPOSITORY = {};


/*
 * @name ACTIVE_ELEMENT_REPOSITORY
 * @desc Central location for all dom element references.
 * @example
 *
 *  element definition:
 *
 *	'<random generated key>': {
 *		element: <dom element>,
 *		type: <element type>,
 *		parent: <key of parent element>,
 *		children: [<children keys>],
 *		attrs: {<element attributes>},
 *		styles: {<instance style properties>},
 *		listeners: [<listeners attached to element>],
 *		position: [<translateX>,<translateY>,<translateZ>],
 *		size: [<width>, <height>],
 *		_dirty: [<changed properties to apply next render cycle>]
 *	}
 *
 */
var ACTIVE_ELEMENT_REPOSITORY = {};


/*
 * @name PENDING_UPDATES_QUEUE
 * @desc Queue of id keys with changed definitions.
 */
var PENDING_UPDATES_QUEUE = [];


/*
 * @name _getRandKey
 * @desc Creates a random string.
 * @return {string} Random string.
 */
var _getRandKey = () => (Math.random() * 1e64).toString(36);


/*
 * @name _activateDefinition
 * @desc Sets key and definition object to ACTIVE_ELEMENT_REPOSITORY.
 * @param {string} key Id of definition object.
 * @param {object} definition Definition object.
 */
var _activateDefinition = (key, definition) => {
		ACTIVE_ELEMENT_REPOSITORY[key] = definition;
	};


/*
 * @name _deactivateDefinition
 * @desc Sets type and definition object to INACTIVE_ELEMENT_REPOSITORY.
 * @param {string} type Type of definition object.
 * @param {object} definition Definition object.
 */
var _deactivateDefinition = (type, definition) => {
		INACTIVE_ELEMENT_REPOSITORY[type] =
			INACTIVE_ELEMENT_REPOSITORY[type] || [];
		INACTIVE_ELEMENT_REPOSITORY[type].push(definition.element);
	};


/*
 * @name _getElement
 * @desc Retrieves element from INACTIVE_ELEMENT_REPOSITORY or creates new.
 * @param {string} type Type of element to retrieve or create.
 * @return {element} Instance of DOMElement
 */
var _getElement = type => {
		return INACTIVE_ELEMENT_REPOSITORY[type] && 
			INACTIVE_ELEMENT_REPOSITORY[type].length > 0 ?
			INACTIVE_ELEMENT_REPOSITORY[type].pop() :
			document.createElement(type);
	};


/*
 * @name _createElementDefinition
 * @desc Creates and registers an active element definition.
 * @param {_Elem} inst Instance of _Elem.
 * @param {string|DOMElement} type Type of element to create | element.
 * @return {string} Id key of definition.
 */
var _createElementDefinition = (inst, type) => {
		var key = _getRandKey();
		var el = typeof type === 'object' ? type : _getElement(type);
		_activateDefinition(key, {
			element: el,
			type: type,
			instance: inst,
			parent: null,
			children: [],
			attrs: {},
			styles: {},
			listeners: [],
			position: [0,0,0],
			size: [0,0],
			_dirty: []
		});
		return key;
	};


/*
 * @name _getDefinition
 * @desc Gets definition object from ACTIVE_ELEMENT_REPOSITORY.
 * @param {string} key Instance _repository_id.
 * @return {object} Definition object.
 */
var _getDefinition = key => {
		return ACTIVE_ELEMENT_REPOSITORY[key];
	};


/*
 * @name _getAction
 * @desc Creates an object for _dirty on definition.
 * @param {string} action CRUD operation.
 * @param {string} property Property on definition.
 * @param {string} keys Keys on property of definition.
 * @return {object} Object formatted for _dirty.
 */
var _getAction = (action, property, keys) => {
		return {
			action:action,
			property:property,
			keys:keys
		};
	};


/*
 * @name _setToPendingUpdatesQueue
 * @desc Appends changed element key to PENDING_UPDATES_QUEUE.
 * @param {string} key Key of changed element definition.
 */
var _setToPendingUpdatesQueue = key => {
		var notAlreadySet = PENDING_UPDATES_QUEUE.indexOf(key) === -1;
		if(notAlreadySet) PENDING_UPDATES_QUEUE.push(key);
	};


/*
 * @name _removeDomNode
 * @desc Removes child node from dom node.
 * @param {DOMElement} parent Parent node.
 * @param {DOMElement} child Child node.
 * @return {DOMElement} Child node.
 */
var _removeDomNode = (parent, child) => parent.removeChild(child);


/*
 * @name _addDomNode
 * @desc Adds child node from dom node.
 * @param {DOMElement} parent Parent node.
 * @param {DOMElement} child Child node.
 * @return {DOMElement} Child node.
 */
var _addDomNode = (parent, child) => parent.appendChild(child);

/*
 * @name _cleanChildren
 * @desc Cleans children property of definition.
 * @param {object} definition Definition to be cleaned.
 * @return {array} Clean array.
 */
var _cleanChildren = definition => {
		definition.children
			.map(childKey => _getDefinition(childKey))
			.forEach(childDef => {
				childDef.parent = undefined;
				_removeDomNode(definition.element, childDef.element);
				childDef.instance.recycle();
			});
		return [];
	};


/*
 * @name _removeDomListener
 * @desc Removes dom event listener.
 * @param {DOMElement} element Element to remove listener from.
 * @param {object} listener Listener definition.
 */
var _removeDomListener = (element, listener) =>
		element.removeEventListener(listener.type, listener.callback);


/*
 * @name _cleanListeners
 * @desc Cleans listeners property of definition.
 * @param {object} definition Definition to be cleaned.
 * @return {array} Clean array.
 */
var _cleanListeners = definition => {
		definition.listeners
			.forEach(listener => {
				_removeDomListener(definition.element, listener);
			});
		return [];
	};


/*
 * @name _cleanAttributes
 * @desc Cleans attrs property of definition.
 * @param {object} definition Definition to be cleaned.
 * @return {object} Clean object.
 */
var _cleanAttributes = definition => {
		for(var prop in definition.attrs) {
			if(definition.attrs.hasOwnProperty(prop)) {
				var el = definition.element;
				el.removeAttribute(prop);
			}
		}
		return {};
	};


/*
 * @name _cleanStyles
 * @desc Cleans styles property of definition.
 * @param {object} definition Definition to be cleaned.
 * @return {object} Clean object.
 */
var _cleanStyles = definition => {
		for(var prop in definition.styles) {
			if(definition.styles.hasOwnProperty(prop)) {
				var el = definition.element;
				el.style[prop] = '';
			}
		}
		return {};
	};


/*
 * @name _cleanPosition
 * @desc Cleans position property of definition.
 * @param {object} definition Definition to be cleaned.
 * @return {array} Clean array.
 */
var _cleanPosition = definition => {
		var el = definition.element;
		el.style.transform = 'translate3d(0,0,0)';
		return [0,0,0];
	};


/*
 * @name _cleanSize
 * @desc Cleans size property of definition.
 * @param {object} definition Definition to be cleaned.
 * @return {array} Clean array.
 */
var _cleanSize = definition => {
		var el = definition.element;
		el.style.width = '0px';
		el.style.height = '0px';
		return [0,0];
	};


/*
 * @name _cleanDefinition
 * @desc Cleans the definition object and sets defaults.
 * @param {string} definition Definition to be cleaned.
 */
var _cleanDefinition = def => {
		_cleanChildren(def);
		_cleanAttributes(def);
		_cleanStyles(def);
		_cleanListeners(def);
		_cleanPosition(def);
		_cleanSize(def);
		def.parent = undefined;
		def.attrs = undefined;
		def.styles = undefined;
		def.listeners = undefined;
		def.position = undefined;
		def.size = undefined;
		def.children = undefined;
		def._dirty = undefined;
		return def;
	};


/*
 * @name _getTranslate3d
 * @desc Returns css transform string for translate3d.
 * @param {array} position Position coordinates for definition.
 * @return {string} Translate3d string.
 */
var _getTranslate3d = position => 
	`translate3d(${position[0]}px, ${position[1]}px, ${position[2]}px)`;


/*
 * @name _processUpdate
 * @desc Applies update to definition and dom element.
 * @param {object} definition Definition object.
 * @param {object} update Pending update definition object.
 */
var _processUpdate = (definition, update) => {
		var action = update.action;
		var property = update.property;
		var key = update.keys;
		switch(property) {
			case 'children':
				if(action === 'update') {
					var childEl = _getDefinition(key).element;
					_addDomNode(definition.element, childEl);
				} else if(action === 'delete') {
					var childEl = _getDefinition(key).element;
					_removeDomNode(definition.element, childEl);
				}
				break;
			case 'attrs':
				if(action === 'update') {
					var el = definition.element;
					el.setAttribute(key, definition.attrs[key]);
				} else if(action === 'delete') {
					var el = definition.element;
					el.removeAttribute(key);
				}
				break;
			case 'styles':
				if(action === 'update') {
					var el = definition.element;
					key.forEach(prop => { 
						el.style[prop] = definition.styles[prop];
					});
				} else if(action === 'delete') {
					var el = definition.element;
					key.forEach(prop => el.style[prop] = '');
				}
				break;
			case 'listeners':
				if(action === 'update') {
					var el = definition.element;
					el.addEventListener(key.type, key.callback);
				} else if(action === 'delete') {
					var el = definition.element;
					el.removeEventListener(key.type, key.callback);
				}
				break;
			case 'position':
				if(action === 'update') {
					var el = definition.element;
					var translate3d = _getTranslate3d(definition.position);
					el.style.transform = translate3d;
				}
				break;
			case 'size':
				if(action === 'update') {
					var el = definition.element;
					var size = definition.size;
					if(key === '0') el.style.width = size[0] + 'px';
					else el.style.height = size[1] + 'px';
				}
				break;
			case 'recycle':
				if(action === 'delete') {
					var cleanDef = _cleanDefinition(definition);
					_deactivateDefinition(definition.type, cleanDef);
				}
				break;
			default:
				console.log(property);
				break;
		}
	};


/*
 * @name _renderUpdates
 * @desc Runs on rAF loop, renders pending updates.
 */
var _renderUpdates = onComplete => {
		PENDING_UPDATES_QUEUE.forEach(key => {
			var def = _getDefinition(key)
			def._dirty.forEach(update => {
				_processUpdate(def, update);
			});
		});
		PENDING_UPDATES_QUEUE = [];
		if(typeof onComplete === 'function') onComplete();
	};


/*
 * @class _Elem
 * @desc Creates an element definition.
 * @param {string|DOMElement} type Type of dom element to create | element.
 * @return {_Elem} Instance of _Elem
 */
function _Elem(type) {
	this._repository_id = _createElementDefinition(this, type);
}

_Elem.prototype = {


	/*
	 * @name getType
	 * @desc Gets type of element from definition.
	 * @return {string} Type of element
	 */
	getType:function() {
		var def = _getDefinition(this._repository_id);
		return def.type;
	},


	/*
	 * @name getParent
	 * @desc Gets the parent of element from definition.
	 * @return {string} Parent id key.
	 */
	getParent:function() {
		var def = _getDefinition(this._repository_id);
		return def.parent;
	},


	/*
	 * @name appendChild
	 * @desc Appends definition children with child id key.
	 * @param {_Elem} elemInstance Instance of child to append.
	 * @return {string} Child id key.
	 */
	appendChild:function(elemInstance) {
		var childKey = elemInstance._repository_id;
		var childDef = _getDefinition(childKey);
		var def = _getDefinition(this._repository_id);
		def.children.push(childKey);
		childDef.parent = this._repository_id;
		def._dirty.push(_getAction('update', 'children', childKey));
		childDef._dirty.push(
			_getAction('update', 'parent', this._repository_id)
		);
		_setToPendingUpdatesQueue(this._repository_id);
		return childKey;
	},


	/*
	 * @name removeChild
	 * @desc Removes definition children with child id key.
	 * @param {_Elem} elemInstance Instance of child to remove.
	 * @return {string} Child id key.
	 */
	removeChild:function(elemInstance) {
		var childKey = elemInstance._repository_id;
		var childDef = _getDefinition(childKey);
		var def = _getDefinition(this._repository_id);
		var childIndex = def.children.indexOf(childKey);
		def.children.splice(childIndex, 1);
		childDef.parent = undefined;
		def._dirty.push(_getAction('delete', 'children', childKey));
		childDef._dirty.push(
			_getAction('delete', 'parent', this._repository_id)
		);
		_setToPendingUpdatesQueue(this._repository_id);
		return childKey;
	},


	/*
	 * @name setAttribute
	 * @desc Sets attribute on definition.
	 * @param {string} key Name of attribute to set.
	 * @param {string} value Value of attribute to set.
	 * @return {string} Value of attribute.
	 */
	setAttribute:function(key, value) {
		var def = _getDefinition(this._repository_id);
		def.attrs[key] = value;
		def._dirty.push(_getAction('update', 'attrs', key));
		_setToPendingUpdatesQueue(this._repository_id);
		return value;
	},


	/*
	 * @name getAttribute
	 * @desc Gets attribute from definition.
	 * @param {string} key Name of attribute to get.
	 * @return {string} Value of attribute.
	 */
	getAttribute:function(key) {
		var def = _getDefinition(this._repository_id);
		return def.attrs[key];
	},


	/*
	 * @name removeAttribute
	 * @desc Removes attribute from definition.
	 * @param {string} key Name of attribute to remove.
	 * @return {string} Value of attribute.
	 */
	removeAttribute:function(key) {
		var def = _getDefinition(this._repository_id);
		var output = def.attrs[key];
		def.attrs[key] = undefined;
		def._dirty.push(_getAction('delete', 'attrs', key));
		_setToPendingUpdatesQueue(this._repository_id);
		return output;
	},


	/*
	 * @name setStyle
	 * @desc Sets style properties on style of definition.
	 * @param {object} properties Style properties to set.
	 * @return {object} Style properties.
	 */
	setStyle:function(properties) {
		var def = _getDefinition(this._repository_id);
		for(var key in properties) {
			if(properties.hasOwnProperty(key)) {
				def.styles[key] = properties[key];
			}
		}
		def._dirty.push(
			_getAction('update', 'styles', Object.keys(properties))
		);
		_setToPendingUpdatesQueue(this._repository_id);
		return properties;
	},


	/*
	 * @name getStyle
	 * @desc Gets style properties from style of definition.
	 * @return {object} Style properties.
	 */
	getStyle:function() {
		var def = _getDefinition(this._repository_id);
		var props = [].slice.call(arguments);
		var output = {};
		props.forEach(prop => {
			output[prop] = def.styles[prop];
		});
		def._dirty.push(_getAction('create', 'styles', props));
		return output;
	},


	/*
	 * @name removeStyle
	 * @desc Removes style properties from style of definition.
	 * @return {object} Style properties.
	 */
	removeStyle:function() {
		var def = _getDefinition(this._repository_id);
		var props = [].slice.call(arguments);
		var output = {};
		props.forEach(prop => {
			output[prop] = def.styles[prop];
			def.styles[prop] = undefined;
		});
		def._dirty.push(_getAction('delete', 'styles', props));
		_setToPendingUpdatesQueue(this._repository_id);
		return output;
	},


	/*
	 * @name addEventListener
	 * @desc Adds an event listener description to listeners of definition.
	 * @param {string} type Type of event.
	 * @param {function} callback Callback of event.
	 */
	addEventListener:function(type, callback) {
		var def = _getDefinition(this._repository_id);
		var evt = {type:type, callback:callback};
		def.listeners.push(evt);
		def._dirty.push(_getAction('update', 'listeners', evt));
		_setToPendingUpdatesQueue(this._repository_id);
		return type;
	},


	/*
	 * @name removeEventListener
	 * @desc Removes an event listener description to listeners of definition.
	 * @param {string} type Type of event.
	 * @param {function} callback Callback of event.
	 */
	removeEventListener:function(type, callback) {
		var def = _getDefinition(this._repository_id);
		var evt = {type:type, callback:callback};
		def.listeners = def.listeners.filter(e => {
			return !(e.type === type && e.callback === callback);
		});
		def._dirty.push(_getAction('delete', 'listeners', evt));
		_setToPendingUpdatesQueue(this._repository_id);
		return type;
	},


	/*
	 * @name setX
	 * @desc Set position x data.
	 * @param {number} value Value of x.
	 * @return {number} Value of x.
	 */
	setX:function(value) {
		var def = _getDefinition(this._repository_id);
		def.position[0] = value;
		def._dirty.push(_getAction('update', 'position', '0'));
		_setToPendingUpdatesQueue(this._repository_id);
		return value;
	},


	/*
	 * @name getX
	 * @desc Get position x data.
	 * @return {number} Value of x.
	 */
	getX:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.position[0];
	},


	/*
	 * @name setY
	 * @desc Set position y data.
	 * @param {number} value Value of y.
	 * @return {number} Value of y.
	 */
	setY:function(value) {
		var def = _getDefinition(this._repository_id);
		def.position[1] = value;
		def._dirty.push(_getAction('update', 'position', '1'));
		_setToPendingUpdatesQueue(this._repository_id);
		return value;
	},


	/*
	 * @name getY
	 * @desc Get position y data.
	 * @return {number} Value of y.
	 */
	getY:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.position[1];
	},


	/*
	 * @name setZ
	 * @desc Set position z data.
	 * @param {number} value Value of z.
	 * @return {number} Value of z.
	 */
	setZ:function(value) {
		var def = _getDefinition(this._repository_id);
		def.position[2] = value;
		def._dirty.push(_getAction('update', 'position', '2'));
		_setToPendingUpdatesQueue(this._repository_id);
		return value;
	},


	/*
	 * @name getZ
	 * @desc Get position z data.
	 * @return {number} Value of z.
	 */
	getZ:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.position[2];
	},


	/*
	 * @name getPosition
	 * @desc Get position data.
	 * @return {array} List of coordinates.
	 */
	getPosition:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.position;
	},


	/*
	 * @name setWidth
	 * @desc Set width data.
	 * @param {number} value Value of width.
	 * @return {number} Value of width.
	 */
	setWidth:function(value) {
		var def = _getDefinition(this._repository_id);
		def.size[0] = value;
		def._dirty.push(_getAction('update', 'size', '0'));
		_setToPendingUpdatesQueue(this._repository_id);
		return value;
	},


	/*
	 * @name getWidth
	 * @desc Get width data.
	 * @return {number} Value of width.
	 */
	getWidth:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.size[0];
	},


	/*
	 * @name setHeight
	 * @desc Set height data.
	 * @param {number} value Value of height.
	 * @return {number} Value of height.
	 */
	setHeight:function(value) {
		var def = _getDefinition(this._repository_id);
		def.size[1] = value;
		def._dirty.push(_getAction('update', 'size', '1'));
		_setToPendingUpdatesQueue(this._repository_id);
		return value;
	},


	/*
	 * @name getHeight
	 * @desc Get height data.
	 * @return {number} Value of height.
	 */
	getHeight:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.size[1];
	},


	/*
	 * @name getSize
	 * @desc Get size data.
	 * @return {array} List of dimensions.
	 */
	getSize:function(value) {
		var def = _getDefinition(this._repository_id);
		return def.size;
	},


	/*
	 * @name recycle
	 * @desc Cleans definition and registers in INACTIVE_ELEMENT_REPOSITORY.
	 */
	recycle:function() {
		var def = _getDefinition(this._repository_id);
		def._dirty.push(_getAction('delete', 'recycle', this._repository_id));
		_setToPendingUpdatesQueue(this._repository_id);
	}

};

module.exports = {
	createElement: type => new _Elem(type),
	renderUpdates: _renderUpdates,
	getInactiveRepo: () => INACTIVE_ELEMENT_REPOSITORY,
	getActiveRepo: () => ACTIVE_ELEMENT_REPOSITORY,
	getUpdateQueue: () => PENDING_UPDATES_QUEUE,
////////////////////////
/* ENABLE FOR TESTING */
////////////////////////
	// _Elem: _Elem,
	// _createElementDefinition: _createElementDefinition,
	// _getDefinition: _getDefinition,
	// _getAction: _getAction
};

////////////////////////
/* ENABLE FOR TESTING */
////////////////////////
// var document = {
// 		createElement: type => {
// 			return { 
// 				type:type,
// 				style:{},
// 				addEventListener: (type, cb) => {},
// 				removeEventListener: (type, cb) => {},
// 				appendChild: child => {},
// 				removeChild: child => {},
// 				setAttribute: (attr, val) => {},
// 				getAttribute: attr => {},
// 				removeAttribute: attr => {}
// 			};
// 		},
// 	};
