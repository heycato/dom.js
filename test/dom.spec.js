var should = require('should');
var dom = require('../dom');

describe('dom', () => {
	describe('element management', () => {

		var key;
		var minKeyLength = 35;

		before(() => {
			key = dom._createElementDefinition('div');
		});

		it('should create a random key', () => {
			should.exist(key);
			(key.length).should.be.above(minKeyLength);
		});

		it('should create and register a definition', () => {
			var definition = dom.getActiveRepo()[key];
			should.exist(definition);
			should.exist(definition.element);
			(definition).should.have.keys([
					'element',
					'type',
					'instance',
					'parent',
					'children',
					'attrs',
					'styles',
					'listeners',
					'position',
					'size',
					'_dirty'
				]);
		});

		describe('createElement', () => {

			var elem;
			var elemDef;
			var child;
			var childDef;
			var definition;

			beforeEach(() => {
				elem = dom.createElement('div');
				elemDef = dom._getDefinition(elem._repository_id);
				child = dom.createElement('div');
				childDef = dom._getDefinition(child._repository_id);
			});

			it('should return an instance of _Elem', () => {
				should.exist(elem);
				(elem instanceof dom._Elem).should.be.true();
			});

			it('should have _repository_id as own property', () => {
				(elem).should.have.ownProperty('_repository_id');
				(elem._repository_id.length).should.be.above(minKeyLength);
			});

			it('should return instance type', () => {
				(elem.getType()).should.equal('div');
			});
			
			describe('parent/children methods', () => {

				beforeEach(() => {
					elem.appendChild(child);
				});

				it('should return instance parent', () => {
					(child.getParent()).should.equal(elem._repository_id);
				});

				it('should add child key to definition children', () => {
					(elemDef.children.indexOf(child._repository_id) > -1)
						.should.be.true();
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'children',
							keys:child._repository_id
						}]);
				});

				it('should add elem key to definition parent', () => {
					(childDef.parent === elem._repository_id).should.be.true();
					childDef._dirty.should.containDeep([{
							action:'update',
							property:'parent',
							keys:elem._repository_id
						}]);
				});

				it('should remove child key from definition children', () => {
					elem.removeChild(child);
					(elemDef.children.indexOf(child._repository_id) > -1)
						.should.be.false();
					(childDef.parent === elem._repository_id).should.be.false();
					elemDef._dirty.should.containDeep([{
							action:'delete',
							property:'children',
							keys:child._repository_id
						}]);
					childDef._dirty.should.containDeep([{
							action:'delete',
							property:'parent',
							keys:elem._repository_id
						}]);
				});

				it('should set to pending update queue', () => {
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});

			});

			describe('attribute methods', () => {

				beforeEach(() => {
					elem.setAttribute('data-id', 'test');
				});

				it('should set attribute', () => {
					(elemDef.attrs['data-id'] === 'test').should.be.true();
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'attrs',
							keys:'data-id'
						}]);
				});

				it('should get attribute', () => {
					(elem.getAttribute('data-id') === 'test').should.be.true();
				});

				it('should remove attribute', () => {
					elem.removeAttribute('data-id');
					(elemDef.attrs['data-id'] === undefined).should.be.true();
					elemDef._dirty.should.containDeep([{
							action:'delete',
							property:'attrs',
							keys:'data-id'
						}]);
				});

				it('should set to pending update queue', () => {
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});

			});

			describe('style methods', () => {

				beforeEach(() => {
					elem.setStyle({
						backgroundColor:'#000000',
						color:'#FFFFFF'
					});
				});

				it('should set style properties', () => {
					(elemDef.styles).should.have.keys([
							'backgroundColor',
							'color'
						]);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'styles',
							keys:['backgroundColor','color']
						}]);
				});

				it('should get style properties', () => {
					var styles = elem.getStyle('backgroundColor', 'color');
					(styles).should.have.keys([
							'backgroundColor',
							'color'
						]);
				});

				it('should remove style properties', () => {
					var styles = elem.removeStyle('backgroundColor', 'color');
					should.not.exist(elemDef.styles.backgroundColor);
					should.not.exist(elemDef.styles.color);
					elemDef._dirty.should.containDeep([{
							action:'delete',
							property:'styles',
							keys:['backgroundColor','color']
						}]);
				});

				it('should set to pending update queue', () => {
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});

			});

			describe('listener methods', () => {

				var handler = e => console.log(e);
				
				beforeEach(() => {
					elem.addEventListener('click', handler); 
				});

				it('should register a listener', () => {
					elemDef.listeners.should.containDeep([{
							type:'click'
						}]);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'listeners',
						}]);
				});

				it('should unregister a listener', () => {
					elem.removeEventListener('click', handler);
					elemDef.listeners.should.not.containDeep([{
							type:'click'
						}]);
					elemDef._dirty.should.containDeep([{
							action:'delete',
							property:'listeners',
						}]);
				});

				it('should set to pending update queue', () => {
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});

			});

			describe('position methods', () => {

				beforeEach(() => {
					elem.setX(50);
					elem.setY(150);
					elem.setZ(300);
				});

				it('should set x position', () => {
					(elemDef.position[0]).should.equal(50);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'position',
							keys:'0'
						}]);
				});

				it('should get x position', () => {
					var x = elem.getX();
					(x).should.equal(50);
				});

				it('should set y position', () => {
					(elemDef.position[1]).should.equal(150);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'position',
							keys:'1'
						}]);
				});

				it('should get y position', () => {
					var y = elem.getY();
					(y).should.equal(150);
				});

				it('should set z position', () => {
					(elemDef.position[2]).should.equal(300);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'position',
							keys:'2'
						}]);
				});

				it('should get z position', () => {
					var z = elem.getZ();
					(z).should.equal(300);
				});

				it('should get position coordinates', () => {
					var coor = elem.getPosition();
					(coor).should.containDeep([50,150,300]);
				});

				it('should set to pending update queue', () => {
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});
			});

			describe('size methods', () => {

				beforeEach(() => {
					elem.setWidth(50);
					elem.setHeight(150);
				});

				it('should set width', () => {
					(elemDef.size[0]).should.equal(50);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'size',
							keys:'0'
						}]);
				});

				it('should get width', () => {
					var w = elem.getWidth();
					(w).should.equal(50);
				});

				it('should set height', () => {
					(elemDef.size[1]).should.equal(150);
					elemDef._dirty.should.containDeep([{
							action:'update',
							property:'size',
							keys:'1'
						}]);
				});

				it('should get height', () => {
					var h = elem.getHeight();
					(h).should.equal(150);
				});

				it('should get dimensions', () => {
					var size = elem.getSize();
					(size).should.containDeep([50,150]);
				});

				it('should set to pending update queue', () => {
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});
			});

			describe('recycle workflow', () => {

				var handler = e => console.log(e);

				before(() => {
					elem.appendChild(child);
					elem.setAttribute('data-id', 'somevalue');
					elem.setStyle({
						backgroundColor:'black',
						color:'white'
					});
					elem.addEventListener('click', handler);
					elem.setX(50);
					elem.setWidth(150);
				});

				it('should recycle definition', () => {
					elem.recycle();	
					
					dom.getUpdateQueue().should.containDeep([
							elem._repository_id
						]);
				});

				it('should execute recycle sequence', done => {
					dom.renderUpdates(done);
					dom.getUpdateQueue().should.be.empty();
				});

			});

			describe('render workflow', () => {

				var handler = e => console.log(e);

				beforeEach(() => {
					elem.appendChild(child);
					elem.setAttribute('data-id', 'somevalue');
					elem.setStyle({
						backgroundColor:'black',
						color:'white'
					});
					elem.addEventListener('click', handler);
					elem.setX(50);
					elem.setWidth(150);
				});

				it('should apply all pending updates', done => {
					dom.renderUpdates(done);

					dom.getUpdateQueue().should.be.empty();
					dom.getInactiveRepo().should.not.be.empty();
				});

			});


		});

	});
});
